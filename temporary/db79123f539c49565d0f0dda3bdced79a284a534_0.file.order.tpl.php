<?php
/* Smarty version 3.1.29, created on 2017-04-11 11:16:18
  from "/home/u347553496/public_html/themes/order.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_58ecf342c41a96_85835467',
  'file_dependency' => 
  array (
    'db79123f539c49565d0f0dda3bdced79a284a534' => 
    array (
      0 => '/home/u347553496/public_html/themes/order.tpl',
      1 => 1490590550,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:scriptolution_error7.tpl' => 1,
    'file:order_scriptolution_localbank.tpl' => 1,
    'file:order_scriptolution_stripe.tpl' => 1,
  ),
),false)) {
function content_58ecf342c41a96_85835467 ($_smarty_tpl) {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:scriptolution_error7.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
  
<?php if ($_smarty_tpl->tpl_vars['enable_paypal']->value == "1" && $_smarty_tpl->tpl_vars['enable_alertpay']->value == "0" && $_smarty_tpl->tpl_vars['funds']->value < $_smarty_tpl->tpl_vars['scriptolution_total_price']->value && $_smarty_tpl->tpl_vars['afunds']->value < $_smarty_tpl->tpl_vars['scriptolution_total_price']->value && $_smarty_tpl->tpl_vars['scriptolutionstripeenable']->value == "0") {?>

<?php echo '<script'; ?>
 type="text/javascript"> 
$(document).ready( function() {
    $('#paypal_form').submit();
});
<?php echo '</script'; ?>
>

<?php } elseif ($_smarty_tpl->tpl_vars['enable_paypal']->value == "0" && $_smarty_tpl->tpl_vars['enable_alertpay']->value == "1" && $_smarty_tpl->tpl_vars['funds']->value < $_smarty_tpl->tpl_vars['scriptolution_total_price']->value && $_smarty_tpl->tpl_vars['afunds']->value < $_smarty_tpl->tpl_vars['scriptolution_total_price']->value && $_smarty_tpl->tpl_vars['scriptolutionstripeenable']->value == "0") {?>

<?php echo '<script'; ?>
 type="text/javascript"> 
$(document).ready( function() {
    $('#alertpay_form').submit();
});
<?php echo '</script'; ?>
>

<?php }?>  
<form action="https://www.paypal.com/cgi-bin/webscr" method="post" id="paypal_form" name="paypal_form">
<input type="hidden" name="cmd" value="_xclick">
<input type="hidden" name="business" value="<?php echo $_smarty_tpl->tpl_vars['paypal_email']->value;?>
">
<input type="hidden" name="item_name" value="#<?php echo stripslashes($_smarty_tpl->tpl_vars['p']->value['PID']);?>
 - <?php echo stripslashes($_smarty_tpl->tpl_vars['p']->value['gtitle']);?>
">
<input type="hidden" name="item_number" value="<?php echo stripslashes($_smarty_tpl->tpl_vars['p']->value['IID']);?>
">
<input type="hidden" name="custom" value="<?php echo $_SESSION['USERID'];?>
">
<input type="hidden" name="amount" value="<?php echo stripslashes($_smarty_tpl->tpl_vars['scriptolution_total_price']->value);?>
">
<input type="hidden" name="currency_code" value="<?php echo $_smarty_tpl->tpl_vars['currency']->value;?>
">
<input type="hidden" name="button_subtype" value="services">
<input type="hidden" name="no_note" value="1">
<input type="hidden" name="no_shipping" value="2">
<input type="hidden" name="rm" value="2">
<input type="hidden" name="return" value="<?php echo $_smarty_tpl->tpl_vars['baseurl']->value;?>
/thank_you?g=<?php echo $_smarty_tpl->tpl_vars['eid']->value;?>
">
<input type="hidden" name="cancel_return" value="<?php echo $_smarty_tpl->tpl_vars['baseurl']->value;?>
/">
<input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynowCC_LG.gif:NonHosted">
<input type="hidden" name="address_override" value="1">
<input type="hidden" name="notify_url" value="<?php echo $_smarty_tpl->tpl_vars['baseurl']->value;?>
/ipn_res.php">
</form>

<form action="" method="post" id="bal_form" name="bal_form">
<input type="hidden" name="subbal" value="1">
</form>                    

<form method="post" action="https://secure.payza.com/checkout" id="alertpay_form" name="alertpay_form">
<input type="hidden" name="ap_merchant" value="<?php echo $_smarty_tpl->tpl_vars['alertpay_email']->value;?>
"/>
<input type="hidden" name="ap_purchasetype" value="service"/>
<input type="hidden" name="ap_itemname" value="#<?php echo stripslashes($_smarty_tpl->tpl_vars['p']->value['PID']);?>
"/>
<input type="hidden" name="ap_amount" value="<?php echo stripslashes($_smarty_tpl->tpl_vars['scriptolution_total_price']->value);?>
"/>
<input type="hidden" name="ap_currency" value="<?php echo $_smarty_tpl->tpl_vars['alertpay_currency']->value;?>
"/>
<input type="hidden" name="ap_quantity" value="1"/>
<input type="hidden" name="ap_itemcode" value="<?php echo $_SESSION['USERID'];?>
"/>
<input type="hidden" name="ap_description" value="<?php echo stripslashes($_smarty_tpl->tpl_vars['p']->value['gtitle']);?>
"/>
<input type="hidden" name="ap_returnurl" value="<?php echo $_smarty_tpl->tpl_vars['baseurl']->value;?>
/thank_you?g=<?php echo $_smarty_tpl->tpl_vars['eid']->value;?>
"/>
<input type="hidden" name="ap_cancelurl" value="<?php echo $_smarty_tpl->tpl_vars['baseurl']->value;?>
/"/>
<input type="hidden" name="apc_1" value="<?php echo stripslashes($_smarty_tpl->tpl_vars['p']->value['IID']);?>
"/>                        
</form>

<form action="" method="post" id="scriptolution_mybal_form" name="scriptolution_mybal_form">
<input type="hidden" name="scriptolution_mybal" value="1">
</form>                              
<div class="bodybg scriptolutionpaddingtop15 scriptolutionopages">
	<div class="whitebody scriptolutionpaddingtop30 scriptolutionwidth842 gray">
		<div class="inner-wrapper scriptolutionwidth842">
			<div class="left-side scriptolutionwidth842">
				<div class="whiteBox twoHalfs padding0 scriptolutionwidth800">
                    
                    <div id="scriptolutionOrderingForm" class="scriptolutionpadding20"> 
                    
                    	<h1><strong><?php echo $_smarty_tpl->tpl_vars['lang550']->value;?>
</strong></h1>
                        <?php if ($_smarty_tpl->tpl_vars['enable_paypal']->value == "1") {?><h2><a style="text-decoration:none" href="#" onclick="document.paypal_form.submit();"><?php echo $_smarty_tpl->tpl_vars['lang411']->value;?>
</a></h2><br /><?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['enable_alertpay']->value == "1") {?><h2><a style="text-decoration:none" href="#" onclick="document.alertpay_form.submit();"><?php echo $_smarty_tpl->tpl_vars['lang447']->value;?>
</a></h2><br /><?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['funds']->value >= $_smarty_tpl->tpl_vars['scriptolution_total_price']->value) {?><h2><a style="text-decoration:none" href="#" onclick="document.bal_form.submit();"><?php echo $_smarty_tpl->tpl_vars['lang412']->value;?>
</a></h2><br /><?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['afunds']->value >= $_smarty_tpl->tpl_vars['scriptolution_total_price']->value) {?><h2><a style="text-decoration:none" href="#" onclick="document.scriptolution_mybal_form.submit();"><?php echo $_smarty_tpl->tpl_vars['lang518']->value;?>
</a></h2><br /><?php }?>
                        <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:order_scriptolution_localbank.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                        <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:order_scriptolution_stripe.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                        <br />
                        
                        <?php if ($_smarty_tpl->tpl_vars['scriptolution_enable_processing_fee']->value == "1") {?>
                        <h3><?php echo $_smarty_tpl->tpl_vars['lang436']->value;?>
: <?php if ($_smarty_tpl->tpl_vars['scriptolution_cur_pos']->value == "1") {
echo $_smarty_tpl->tpl_vars['scriptolution1price']->value;
echo $_smarty_tpl->tpl_vars['lang197']->value;
} else {
echo $_smarty_tpl->tpl_vars['lang197']->value;
echo $_smarty_tpl->tpl_vars['scriptolution1price']->value;
}?></h3>
                        <h3><?php echo $_smarty_tpl->tpl_vars['lang652']->value;?>
: <?php if ($_smarty_tpl->tpl_vars['scriptolution_cur_pos']->value == "1") {
echo $_smarty_tpl->tpl_vars['scriptolution_total_fees']->value;
echo $_smarty_tpl->tpl_vars['lang197']->value;
} else {
echo $_smarty_tpl->tpl_vars['lang197']->value;
echo $_smarty_tpl->tpl_vars['scriptolution_total_fees']->value;
}?></h3>                        
                        <h2><?php echo $_smarty_tpl->tpl_vars['lang489']->value;?>
: <?php if ($_smarty_tpl->tpl_vars['scriptolution_cur_pos']->value == "1") {
echo $_smarty_tpl->tpl_vars['scriptolution_total_price']->value;
echo $_smarty_tpl->tpl_vars['lang197']->value;
} else {
echo $_smarty_tpl->tpl_vars['lang197']->value;
echo $_smarty_tpl->tpl_vars['scriptolution_total_price']->value;
}?></h2>
                        <?php } else { ?>
                        <h2><?php echo $_smarty_tpl->tpl_vars['lang489']->value;?>
: <?php if ($_smarty_tpl->tpl_vars['scriptolution_cur_pos']->value == "1") {
echo $_smarty_tpl->tpl_vars['scriptolution1price']->value;
echo $_smarty_tpl->tpl_vars['lang197']->value;
} else {
echo $_smarty_tpl->tpl_vars['lang197']->value;
echo $_smarty_tpl->tpl_vars['scriptolution1price']->value;
}?></h2>
                        <?php }?>
                        
                    </div>

					<div class="clear"></div>
				</div>
			</div>			
			<div class="clear"></div>
		</div>   
	</div>
</div>
<div id="scriptolutionnobottom">
    <div class="centerwrap footertop">
        <div class="footerbg gray scriptolutionfooter842"></div>
    </div>
</div><?php }
}
