<?php
/* Smarty version 3.1.29, created on 2017-04-24 08:27:34
  from "/home/u347553496/public_html/themes/scriptolution_header_launch.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_58fdef36924410_53300375',
  'file_dependency' => 
  array (
    '72191e64db15e55d486dc5d2ec2508ef1e1664f1' => 
    array (
      0 => '/home/u347553496/public_html/themes/scriptolution_header_launch.tpl',
      1 => 1493036825,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:scriptolution_rtl2.tpl' => 1,
  ),
),false)) {
function content_58fdef36924410_53300375 ($_smarty_tpl) {
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml" xmlns:fb="https://www.facebook.com/2008/fbml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php if ($_smarty_tpl->tpl_vars['mtitle']->value != '') {
echo $_smarty_tpl->tpl_vars['mtitle']->value;
} else {
if ($_smarty_tpl->tpl_vars['pagetitle']->value != '') {
echo $_smarty_tpl->tpl_vars['pagetitle']->value;?>
 - <?php }
echo $_smarty_tpl->tpl_vars['site_name']->value;
}?></title>
	<meta name="description" content="<?php if ($_smarty_tpl->tpl_vars['mdesc']->value != '') {
echo $_smarty_tpl->tpl_vars['mdesc']->value;
} else {
if ($_smarty_tpl->tpl_vars['pagetitle']->value != '') {
echo $_smarty_tpl->tpl_vars['pagetitle']->value;?>
 - <?php }
if ($_smarty_tpl->tpl_vars['metadescription']->value != '') {
echo $_smarty_tpl->tpl_vars['metadescription']->value;?>
 - <?php }
echo $_smarty_tpl->tpl_vars['site_name']->value;
}?>">
	<meta name="keywords" content="<?php if ($_smarty_tpl->tpl_vars['mtags']->value != '') {
echo $_smarty_tpl->tpl_vars['mtags']->value;
} else {
if ($_smarty_tpl->tpl_vars['pagetitle']->value != '') {
echo $_smarty_tpl->tpl_vars['pagetitle']->value;?>
,<?php }
if ($_smarty_tpl->tpl_vars['metakeywords']->value != '') {
echo $_smarty_tpl->tpl_vars['metakeywords']->value;?>
,<?php }
echo $_smarty_tpl->tpl_vars['site_name']->value;
}?>"> 
    <link href="<?php echo $_smarty_tpl->tpl_vars['baseurl']->value;?>
/css/scriptolution_style_v7.css" media="screen" rel="stylesheet" type="text/css" /> 
    <link href="<?php echo $_smarty_tpl->tpl_vars['baseurl']->value;?>
/css/scriptolutionresponse.css" media="screen" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"> 
    <?php echo '<script'; ?>
 type="text/javascript">
    var base_url = "<?php echo $_smarty_tpl->tpl_vars['baseurl']->value;?>
";
	<?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 type="text/javascript" src="https://apis.google.com/js/plusone.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
>
        sQuery = jQuery.noConflict(true);
    <?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" type="text/javascript"><?php echo '</script'; ?>
>	
    <link rel="icon" href="<?php echo $_smarty_tpl->tpl_vars['baseurl']->value;?>
/favicon.ico" />
    <link rel="shortcut icon" href="<?php echo $_smarty_tpl->tpl_vars['baseurl']->value;?>
/favicon.ico" />
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
	<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['baseurl']->value;?>
/js/scriptolution.js"><?php echo '</script'; ?>
>
    <?php if ($_smarty_tpl->tpl_vars['rtl']->value == "1") {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:scriptolution_rtl2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}?>
</head>
<body>
    <div id="loadme"></div>
    <?php if ($_smarty_tpl->tpl_vars['enable_fc']->value == "1") {?>
    <div id="fb-root"></div>
    
    <?php echo '<script'; ?>
 src="https://connect.facebook.net/en_US/all.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
>
      FB.init({appId: '<?php echo $_smarty_tpl->tpl_vars['FACEBOOK_APP_ID']->value;?>
', status: true,
               cookie: true, xfbml: true});
      FB.Event.subscribe('auth.login', function(response) {
      });	  
    <?php echo '</script'; ?>
>
    
    <?php }?>
    <div class="header" style="height:54px !important;">
        <div class="centerwrap relative">
            <div class="headertop">
                <div class="logo"><!-- <a href="<?php echo $_smarty_tpl->tpl_vars['baseurl']->value;?>
/"><img src="<?php echo $_smarty_tpl->tpl_vars['imageurl']->value;?>
/scriptolution_logo.png" alt="<?php echo $_smarty_tpl->tpl_vars['site_name']->value;?>
" /></a> --></div>        	
                <div class="clear"></div>
            </div>
        </div>
    </div>
    <div style="clear:both"></div>   <?php }
}
