<?php
/* Smarty version 3.1.29, created on 2017-04-24 05:37:37
  from "/home/u347553496/public_html/themes/scriptolution_footer.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_58fdc761ce28c3_17542882',
  'file_dependency' => 
  array (
    'd016ee80d381c427050b590d70cf83492dffe023' => 
    array (
      0 => '/home/u347553496/public_html/themes/scriptolution_footer.tpl',
      1 => 1493026654,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:scriptolution_po.tpl' => 1,
    'file:lang.tpl' => 1,
  ),
),false)) {
function content_58fdc761ce28c3_17542882 ($_smarty_tpl) {
?>
<div class="footer">
	<div class="centerwrap footertop">
    	<div class="footerbg"></div>
    <!-- 	<div class="flogo"><a href="<?php echo $_smarty_tpl->tpl_vars['baseurl']->value;?>
/"><img src="<?php echo $_smarty_tpl->tpl_vars['imageurl']->value;?>
/scriptolution_footer_logo.png" alt="scriptolution" /></a></div> -->
      	<!-- <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:scriptolution_po.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
 -->
        <div class="bottomlink">
        	<ul>
            	<li><a href="<?php echo $_smarty_tpl->tpl_vars['baseurl']->value;?>
/terms_of_service"><?php echo $_smarty_tpl->tpl_vars['lang253']->value;?>
</a></li>
                <li><a href="<?php echo $_smarty_tpl->tpl_vars['baseurl']->value;?>
/privacy_policy"><?php echo $_smarty_tpl->tpl_vars['lang415']->value;?>
</a></li>
                <li><a href="<?php echo $_smarty_tpl->tpl_vars['baseurl']->value;?>
/contact"><?php echo $_smarty_tpl->tpl_vars['lang417']->value;?>
</a></li>
            </ul>
            <ul>
            	<li><a href="<?php echo $_smarty_tpl->tpl_vars['baseurl']->value;?>
/about"><?php echo $_smarty_tpl->tpl_vars['lang416']->value;?>
</a></li>
                <li><a href="<?php echo $_smarty_tpl->tpl_vars['baseurl']->value;?>
/advertising"><?php echo $_smarty_tpl->tpl_vars['lang418']->value;?>
</a></li>
                <?php if ($_smarty_tpl->tpl_vars['enable_levels']->value == "1" && $_smarty_tpl->tpl_vars['price_mode']->value == "3") {?><li><a href="<?php echo $_smarty_tpl->tpl_vars['baseurl']->value;?>
/levels"><?php echo $_smarty_tpl->tpl_vars['lang500']->value;?>
</a></li><?php }?>
            </ul>
        </div>
        <div class="clear"></div>
    </div>
    <div class="scriptolutionfooterlang">
    <center><?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:lang.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
</center>
    </div>
</div>
</body>
</html><?php }
}
