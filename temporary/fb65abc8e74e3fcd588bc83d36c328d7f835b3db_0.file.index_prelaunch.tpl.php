<?php
/* Smarty version 3.1.29, created on 2017-04-10 09:22:25
  from "/home/u347553496/public_html/themes/index_prelaunch.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_58eb8711e413e6_70044746',
  'file_dependency' => 
  array (
    'fb65abc8e74e3fcd588bc83d36c328d7f835b3db' => 
    array (
      0 => '/home/u347553496/public_html/themes/index_prelaunch.tpl',
      1 => 1490590550,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:scriptolution_error7.tpl' => 1,
  ),
),false)) {
function content_58eb8711e413e6_70044746 ($_smarty_tpl) {
?>

<style>
.whiteBox h4 {
    float: left;
    width: 100%;
    font-size: 18px;
    font-family: 'latobold', sans-serif;
    color: #424242;
    margin-bottom: 15px;
}
.launchfooter p {
    font-family: 'latoregular';
    font-size: 14px;
    color: #A39E9E;
    text-align: center;
    padding-top: 24px;
}
.launchfooter p a{color:#38b0d9;}
.launchfooter p a:hover{color:#666;}
#scriptolutionForm label {
    color: #38b0d9 !important;
}
</style>

	<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:scriptolution_error7.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <div class="bodybg scriptolutionpaddingtop15 scriptolutionloginpage">
        <div class="whitebody scriptolutionpaddingtop30 scriptolutionwidth482">
            <div class="inner-wrapper scriptolutionwidth442">
                <div class="left-side scriptolutionwidth442">
                    <div class="whiteBox twoHalfs padding15 scriptolutionwidth400">
                        <h1><?php echo $_smarty_tpl->tpl_vars['lang597']->value;?>
...</h1>
                        <h4><?php echo $_smarty_tpl->tpl_vars['lang598']->value;?>
</h4>
                        <div class="scriptolutionpaddingtop15"></div>
                        <div id="scriptolutionForm">
                            <form action="<?php echo $_smarty_tpl->tpl_vars['baseurl']->value;?>
/index.php" method="post">  
                                <div class="form-entry">
                                    <label for="l_username">* <?php echo $_smarty_tpl->tpl_vars['lang600']->value;?>
</label>
                                    <input class="text" name="scriptolutionemail" size="16" tabindex="1" type="text" value="" placeholder="<?php echo $_smarty_tpl->tpl_vars['lang601']->value;?>
" />
                                </div>
                                <div class="row">
                                    <input type="submit" value="<?php echo $_smarty_tpl->tpl_vars['lang599']->value;?>
" class="scriptolutionbluebutton" />
                                    <input type="hidden" name="jscriptolution" id="jscriptolution" value="1" />
                                </div>
                            </form>   
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>			
                <div class="clear"></div>
                <div id="scriptolutionFormLinks">
                    <div class="scriptolutionloginsignuplink">
                        <!-- social -->
                    </div>
                </div>
            </div>   
        </div>
    </div>
    
    <div id="scriptolutionnobottom">
        <div class="centerwrap footertop">
            <div class="footerbg scriptolutionfooter482"></div>
        </div>
    </div><?php }
}
