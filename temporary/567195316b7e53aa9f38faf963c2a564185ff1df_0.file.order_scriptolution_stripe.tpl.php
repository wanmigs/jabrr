<?php
/* Smarty version 3.1.29, created on 2017-04-11 11:16:18
  from "/home/u347553496/public_html/themes/order_scriptolution_stripe.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_58ecf342ccfb21_33458315',
  'file_dependency' => 
  array (
    '567195316b7e53aa9f38faf963c2a564185ff1df' => 
    array (
      0 => '/home/u347553496/public_html/themes/order_scriptolution_stripe.tpl',
      1 => 1490590550,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_58ecf342ccfb21_33458315 ($_smarty_tpl) {
if (!is_callable('smarty_function_math')) require_once '/home/u347553496/public_html/scriptolution/libs/plugins/function.math.php';
if ($_smarty_tpl->tpl_vars['scriptolutionstripeenable']->value == "1") {
echo '<script'; ?>
 src="https://checkout.stripe.com/checkout.js"><?php echo '</script'; ?>
>
<h2>
	<a id="customButton" style="text-decoration:none" href="#"><?php echo $_smarty_tpl->tpl_vars['lang612']->value;?>

    <br />
	<i class="fa fa-cc-visa fa-2x"></i>
    <i class="fa fa-cc-mastercard fa-2x"></i>
    <i class="fa fa-cc-amex fa-2x"></i>
    <i class="fa fa-cc-discover fa-2x"></i>
    <i class="fa fa-cc-diners-club fa-2x"></i>
    <i class="fa fa-cc-jcb fa-2x"></i>
    </a>
</h2>
<?php echo '<script'; ?>
>
  var handler = StripeCheckout.configure({
    key: '<?php echo $_smarty_tpl->tpl_vars['scriptolutionstripepublishable']->value;?>
',
    //image: '/img/documentation/checkout/marketplace.png',
    locale: 'auto',
    token: function(token) {
      // Use the token to create the charge with a server-side script.
      // You can access the token ID with `token.id`
	  window.location.href = '<?php echo $_smarty_tpl->tpl_vars['baseurl']->value;?>
/order.php?item=<?php echo stripslashes($_smarty_tpl->tpl_vars['p']->value['IID']);?>
&scriptolutionstripe=1&scriptolutionencoded=<?php echo $_smarty_tpl->tpl_vars['scriptolutionencoded']->value;?>
&token='+token.id
    }
  });
  sQuery('#customButton').on('click', function(e) {
    // Open Checkout with further options
    handler.open({
      name: '<?php echo $_smarty_tpl->tpl_vars['site_name']->value;?>
',
      description: '#<?php echo stripslashes($_smarty_tpl->tpl_vars['p']->value['PID']);?>
 - <?php echo stripslashes($_smarty_tpl->tpl_vars['p']->value['gtitle']);?>
',
      currency: "<?php echo $_smarty_tpl->tpl_vars['scriptolutionstripecurrency']->value;?>
",
	  email: "<?php echo stripslashes($_smarty_tpl->tpl_vars['scriptolutionuemail']->value);?>
",
	  <?php if ($_smarty_tpl->tpl_vars['scriptolution_enable_processing_fee']->value == "1") {?>
	  amount: "<?php echo smarty_function_math(array('equation'=>'x * 100','x'=>$_smarty_tpl->tpl_vars['scriptolution_total_price']->value),$_smarty_tpl);?>
"
	  <?php } else { ?>
	  amount: "<?php echo smarty_function_math(array('equation'=>'x * 100','x'=>$_smarty_tpl->tpl_vars['scriptolution1price']->value),$_smarty_tpl);?>
"	  
	  <?php }?>
    });
    e.preventDefault();
  });
  // Close Checkout on page navigation
  sQuery(window).on('popstate', function() {
    handler.close();
  });
<?php echo '</script'; ?>
>
<?php }
}
}
